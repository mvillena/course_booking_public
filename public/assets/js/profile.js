// get the value of the access token inside the local storage and place it inside a new variable.

let token = localStorage.getItem("token");
console.log(token);

//Selectors
let header = document.querySelector('.header');
let hamburgerMenu = document.querySelector('.hamburger-menu');


window.addEventListener('scroll', (e) => {
	let windowPosition = window.scrollY > 500;
	header.classList.toggle('active', windowPosition);
})

hamburgerMenu.addEventListener('click', () => {
	header.classList.toggle('menu-open');
})

let lalagyan = document.querySelector("#profileContainer")
// our goal here is to display the information about the user details
// send a request to the backend project
fetch('https://polar-meadow-85913.herokuapp.com/api/users/details', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
}).then(res => res.json())
.then(jsonData => {

	let userSubjects = jsonData.enrollments.map(subject => {

		// group activity code:
		fetch(`https://polar-meadow-85913.herokuapp.com/api/courses/
			${subject.courseId}`).then(res => res.json()).then(convertedData => {

				const mergedObject = {...subject, ...convertedData}

				document.getElementById('container').innerHTML +=

					`
					<tr>
						<td> ${mergedObject.name}</td>
						<td> ${mergedObject.description}</td>
						<td> ${mergedObject.enrolledOn}</td>
						<td> ${mergedObject.status}</td>
					</tr>
					`


		})

		}).join("")		
	
	

	 lalagyan.innerHTML =`
		<center>
		<div class="col-md-13 profile">
			<section class="jumbotron my-5 ">
				<h3 class="text-center">First Name: ${jsonData.firstName}</h3>
				<h3 class="text-center">Last Name: ${jsonData.lastName}</h3>
				<h3 class="text-center">Email: ${jsonData.email}</h3>
				<h3 class="text-center">Mobile Number: ${jsonData.mobileNo}</h3>
				<table class="table">
					<tr>
						<th>Course Name: </th>
						<th>Course Description: </th>
						<th>Enrolled On: </th>
						<th>Status: </th>
						<tbody id="container">  </tbody>
					</tr>
				</table>

			</section>
		</div>
		</center>
	`
})