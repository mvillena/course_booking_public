// console.log("hello")

//Lets identify which course this page needs to display inside the browser

//earlier we passed the course id data inside the parameters of the url for us to quickly identify which course to display.

//URLSearchParams() -> this method/constructor creates and returns a URLSearchParams object.

//window.location - returns a location object with information about the "current location" of the document.
//.search -> contains the query string section of the current url.

//Selectors
let header = document.querySelector('.header');
let hamburgerMenu = document.querySelector('.hamburger-menu');


window.addEventListener('scroll', (e) => {
	let windowPosition = window.scrollY > 500;
	header.classList.toggle('active', windowPosition);
})

hamburgerMenu.addEventListener('click', () => {
	header.classList.toggle('menu-open');
})

let urlValues = new URLSearchParams(window.location.search)

//lets check what the structure of this variable will look like
// console.log(urlValues)

let id = urlValues.get('courseId')
console.log(id)

//capture the value of the access token inside the local storage
let token = localStorage.getItem('token')
// console.log(token) - checker

//set a container for all the details that we would want to display.

let name = document.querySelector("#courseName")
let price = document.querySelector("#coursePrice")
let desc = document.querySelector("#courseDesc")
let enroll = document.querySelector("#enrollmentContainer")


fetch(`https://polar-meadow-85913.herokuapp.com/api/courses/${id}`).then(res => res.json()).then(convertedData => {
	console.log(convertedData)
	let idOfTheCourse = convertedData._id
	name.innerHTML =	convertedData.name
	price.innerHTML =	convertedData.price
	desc.innerHTML =	convertedData.description
	enroll.innerHTML = `<a id="enrollButton" class="btn btn-success text-white btn-block">Enroll</a>`

	document.querySelector("#enrollButton").addEventListener('click', () => {
		fetch('https://polar-meadow-85913.herokuapp.com/api/users/details', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
		}).then(res => res.json())
		.then(jsonData => {

			let enrollmentsArr = jsonData.enrollments
			let tempArr = []

			enrollmentsArr.map(coursesEnrolled => {
				let courseEnrolledId = coursesEnrolled.courseId
				tempArr.push(courseEnrolledId)
				console.log(tempArr)
			}).join("")//end of courseEnrolled arrow notation

			if(tempArr.indexOf(idOfTheCourse) === -1) {
				console.log("Can Still Enroll")
				console.log(tempArr)



			//send a request going to the endpoint that will allow us to get and display the course details

			fetch(`https://polar-meadow-85913.herokuapp.com/api/courses/${id}`).then(res => res.json()).then(convertedData => {
				console.log(convertedData)

				name.innerHTML =	convertedData.name
				price.innerHTML =	convertedData.price
				desc.innerHTML =	convertedData.description
				enroll.innerHTML = `<a id="enrollButton" class="btn btn-success text-white btn-block">Enroll</a>`

				document.querySelector("#enrollButton").addEventListener('click', () => {

					fetch('https://polar-meadow-85913.herokuapp.com/api/users/enroll', {
						method: 'POST',
						headers: {
							'Content-Type': 'application/json',
							'Authorization': `Bearer ${token}` //have to get the actual value of the token variable
						},
						body: JSON.stringify({
							courseId: id
						})
					}).then(res=> {
						return res.json()
					}).then(convertedResponse => {
						console.log(convertedResponse)

						if (convertedResponse === true) {
							alert('Course Enrolled Successfully!')
							window.location.replace('./courses.html')
						} else {
							Swal.fire({
			   	  			icon: "error",
							text: "Enrollment Failed!"
			   	  		});	
						}
						// }
					})//end for enroll
				})//end of event
			})//end of fetch for course

		}else{
			alert('Course Already Enrolled!')
		}
	})
  })
})