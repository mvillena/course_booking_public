//target the container from the html document.

let adminControls = document.querySelector('#adminButton')

let container = document.querySelector('#coursesContainer')


//lets determine if there is a user currently logged in.
//lets capture one of the properties that is currently stored in our web storage
const isAdmin = localStorage.getItem("isAdmin")
//let's create a control structure to determine the display in the front end

//Selectors
let header = document.querySelector('.header');
let hamburgerMenu = document.querySelector('.hamburger-menu');


window.addEventListener('scroll', (e) => {
	let windowPosition = window.scrollY > 500;
	header.classList.toggle('active', windowPosition);
})

hamburgerMenu.addEventListener('click', () => {
	header.classList.toggle('menu-open');
})

if (isAdmin =="false" || !isAdmin) {
	
	adminControls.innerHTML = null;

} else {
	adminControls.innerHTML = 
	`
	<div class="col-md-2 offset-md-10">
		<a href="./../../public/pages/addCourse.html" class="btn btn-block btn-warning">Add Course</a>
	</div>
	`
}

//send a request to retrieve all documents from the courses collection
fetch('https://polar-meadow-85913.herokuapp.com/api/courses/').then(res => res.json()).then(jsonData => {
	console.log(jsonData) //only inserted as a checker

	//lets declare a variable that will display the result in the browser depending on the return.

	let courseData;

	//create a control structure that will determine the value that the variable will hold.

	if (jsonData.length < 1) {
		console.log("No Courses Available")
		courseData = "No Courses Available"
		container.innerHTML = courseData
	} else {
		//if the condition given is not met, display the contents of the array inside our page.

		//we will iterate the courses collection and display each course inside the browser.
		courseData = jsonData.map(course => {
			//lets check the make up/structure of each element inside the courses collection.

			console.log(course)


			//ANSWER KEY:

			let cardFooter;
			
			if (isAdmin =="false" || !isAdmin) {
				cardFooter = 
				`
				<a href="./course.html?courseId=${course._id}" class="btn btn-success text-white btn-block">View Course Details</a>
				`
			} else {
				cardFooter = 

				`
				<a href="" class="btn btn-warning text-white btn-block">Edit Course</a>
				<a href="" class="btn btn-danger text-white btn-block">Delete Course</a>
				`
			}

			return (
				`
				<div class="col-md-6 my-3">
					<div class="card">
						<div class="card-body">
							<h3 class="card-title">Course Name: ${course.name}</h3>
							<p class="card-text text-left">Price: ${course.price}</p>
							<p class="card-text text-left">Description: ${course.description}</p>
							<p class="card-text text-left">Created On: ${course.createdOn}</p>
						</div>
						<div class="card-footer">
							${cardFooter}
						</div>
					</div>
				</div>
			`
				)


			//MY ANSWER:
			/*if (isAdmin =="false" || !isAdmin) {

			return (
				`
				<div class="col-md-6 my-3">
					<div class="card">
						<div class="card-body">
							<h3 class="card-title">Course Name: ${course.name}</h3>
							<p class="card-text text-left">Price: ${course.price}</p>
							<p class="card-text text-left">Description: ${course.description}</p>
							<p class="card-text text-left">Created On: ${course.createdOn}</p>
						</div>
						<div class="card-footer">
							<a href="#" class="btn btn-success text-white btn-block">View Course Details</a>
						</div>
					</div>
				</div>
			`
				)
		}
		else {

			return (
			`
				<div class="col-md-6 my-3">
					<div class="card">
						<div class="card-body">
							<h3 class="card-title">Course Name: ${course.name}</h3>
							<p class="card-text text-left">Price: ${course.price}</p>
							<p class="card-text text-left">Description: ${course.description}</p>
							<p class="card-text text-left">Created On: ${course.createdOn}</p>
						</div>
						<div class="card-footer">
							<a href="#" class="btn btn-warning text-white btn-block">Edit Course</a>
							<a href="#" class="btn btn-warning text-white btn-block">Delete Course</a>
						</div>
					</div>
				</div>
			`
			)
		}*/

		}).join("")
			container.innerHTML = courseData;
	}
})

