// console.log("hello from JS file"); 

//lets target first our form component and place it inside a new variable
let registerForm = document.querySelector("#registerUser")

let header = document.querySelector('.header');
let hamburgerMenu = document.querySelector('.hamburger-menu');


window.addEventListener('scroll', (e) => {
   let windowPosition = window.scrollY > 500;
   header.classList.toggle('active', windowPosition);
})

hamburgerMenu.addEventListener('click', () => {
   header.classList.toggle('menu-open');
})

//inside the first param of the method describe the event that it will listen to, while inside the 2nd parameter lets describe the action/ procedure that will happen upon triggering the said event. 
registerForm.addEventListener("submit", (pangyayari) => {
    pangyayari.preventDefault() //to avoid page refresh/page redirection once that the said event has been triggered. 

    //capture each values inside the input fields first, then repackage them inside a new variable
	let fName = document.querySelector("#firstName").value
	//lets create a checker to make sure that we are successful in captring the values.
	//console.log(firstName)
	let lastName = document.querySelector("#lastName").value
	//lets create a checker to make sure that we are successful in captring the values.
	//console.log(lastName)
	let email = document.querySelector("#userEmail").value
	//lets create a checker to make sure that we are successful in captring the values.
	//console.log(email)
	let mobileNo = document.querySelector("#mobileNumber").value
	//lets create a checker to make sure that we are successful in captring the values.
	//console.log(mobileNo)
	let password = document.querySelector("#password1").value
	//lets create a checker to make sure that we are successful in captring the values.
	//console.log(password)
	let verifyPassword = document.querySelector("#password2").value
	//lets create a checker to make sure that we are successful in captring the values.
	//console.log(verifyPassword)
    

   //lets create a data validation for our register page. WHY?
   ///why do we need to validate data? ..to check and verify if the data that we will accept is accurate. 
   //we do data validation to make sure that the storage space will be properly utilizes.

   //the folllowing info/data that we can validate 
   //email, password. mobileNo. 
   //lets create a control structure to determine the next set of procedures before the user can register a new account.
   //it will be a lot more efficient for you to sanitize the data before submitting it to the backend/server for processing

   if (password !== verifyPassword) {
   		Swal.fire({
   	  			icon: "error",
				text: "Password Not Matched!"
   	  		})
   }

   else if (password.length < 8) {
   		Swal.fire({
   	  			icon: "error",
				text: "Password Should be within 8-15 characters!"
   	  		});
   }

   else if (password.length > 15) {
   		Swal.fire({
   	  			icon: "error",
				text: "Password Should be within 8-15 characters!"
   	  		});
   }

   else if (password.search(/[0-9]/) < 0) {
   		Swal.fire({
   	  			icon: "error",
				text: "Password Should have at least one number!"
   	  		});
   }

   else if (password.search(/[A-Z]/) < 0) {
   		Swal.fire({
   	  			icon: "error",
				text: "Password Should have at least one capital letter!"
   	  		});
   }

   else if (password.search(/[a-z]/) < 0) {
   		Swal.fire({
   	  			icon: "error",
				text: "Password Should have at least one small letter!"
   	  		});
   }

   else if (password.search(/[!@#$%^&*]/) < 0) {
   		Swal.fire({
   	  			icon: "error",
				text: "Password Should have at least one special character!"
   	  		});
   }

   else if((fName  !== "" && lastName !== ""  && email !== ""  && password !== ""  && verifyPassword !== "" ) && (password === verifyPassword) && (mobileNo.length === 11))
   {
   	  // before you allow the user to create a new account check if the email value is still available for use. This will ensure that each user will have their unique user email.
   	  fetch('https://polar-meadow-85913.herokuapp.com/api/users/email-exists', {
   	  	method: 'POST',
   	  	headers: {
   	  		'Content-Type': 'application/json'
   	  	},
   	  	body:  JSON.stringify({
   	  		email: email
   	  	})
   	  }).then(res => {
   	  	return res.json() /*to make it readable once the response returns to the client side*/
   	  }).then(convertedData => {


   	  	if (convertedData === false) {

   	  		fetch('https://polar-meadow-85913.herokuapp.com/api/users/register', {
      	//we will now describe the structure of our request for register
      	method: 'POST',
      	headers: {
      		'Content-Type': 'application/json'
      	},
      	//API only accepts request in a string format. 
      	body: JSON.stringify({
      		firstName: fName,
      		lastName: lastName,
      		email: email,
      		mobileNo: mobileNo, 
      		password: password
      	}) 
      }).then(res => {

      	console.log(res)
      	//console.log("hello");
      	return res.json()
      }).then(data => {
      	console.log(data)
      	//lets create here a control structure to give out a proper response depending on the return from the backend. 
      	if(data === true){
           Swal.fire(
			  'Good job!',
			  'Account Created Successfully!',
			  'success'
			)
      	}else{
           //inform the user that something went wrong
            Swal.fire("Something with wrong during processing!")
      	}
      })
   	  	}else {
   	  		Swal.fire({
   	  			icon: "error",
				text: "Email Already Exists!"
   	  		})
   	  	}
   	  })

   	  //this block of code will run if the condition has been met
      //how can we create a new account for user using the data that he/she entered?
      //url -> describes the destination of the request. 
      //3 STATES for a promise (pending, fullfillment, rejected)
      

   }else{
   Swal.fire("Oh no!!!")
}
}) 

