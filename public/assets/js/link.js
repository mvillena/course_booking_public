//Selectors
let header = document.querySelector('.header');
let hamburgerMenu = document.querySelector('.hamburger-menu');


window.addEventListener('scroll', (e) => {
	let windowPosition = window.scrollY > 500;
	header.classList.toggle('active', windowPosition);
})

hamburgerMenu.addEventListener('click', () => {
	header.classList.toggle('menu-open');
})