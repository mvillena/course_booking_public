/*console.log("hello")*/

// Lets target our form component inside our document.
let loginForm = document.querySelector('#loginUser')


// the login form will be used by the client to insert his/her account to be authenticated by the app.
loginForm.addEventListener("submit", (event) => {
	event.preventDefault()

	// lets capture the values our form components.
	let email = document.querySelector("#userEmail").value
	let pass = document.querySelector("#password").value

	// create a checker to confirm the acquired values
	// console.log(email)
	// console.log(pass)

	// validate the data inside the input fields first.
	if (email == "" || pass == "") 
	{
		Swal.fire({
   	  			icon: "error",
				text: "Input Email/Password!"
   	  		})

	} else 
	{
		// send a request to the desired endpoint.
		fetch('https://polar-meadow-85913.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: pass
			})
		}).then(res => {
			return res.json()
		}).then(dataConverted => {
			/*console.log(dataConverted)*/
			// save generated access token inside the local storage property of the browser.
			// localStorage.setItem(key, value)
			/*console.log(dataConverted)*/
			if (dataConverted.accessToken) {
				localStorage.setItem('token', dataConverted.accessToken)
				/*Swal.fire(
					  'Good job!',
					  'Successfully Generated Access Token!',
					  'success'
					);*/
					// Make sure that before you redirect the user to the next location you have to identify the access rights that you will grant for that user.
				fetch('https://polar-meadow-85913.herokuapp.com/api/users/details', {
					headers: {
						// lets pass on the value of our access token
						'Authorization': `Bearer ${dataConverted.accessToken}`
					}   /*upon sending this request we are instantiating a promise that can lead to 2 possible outcomes*/
				}).then( res => {
					return res.json()
				}).then( data => {
					// console.log(data);
					// save id and isAdmin in local storage
					localStorage.setItem("id", data._id)
					localStorage.setItem("isAdmin", data.isAdmin)

					// console.log("items are set inside the local storage")
					
					window.location.replace('./profile.html')
				})

				
			}else {
				Swal.fire({
   	  			icon: "error",
				text: "Not a valid account!"
   	  		})

				/*window.location.replace('./login.html')*/
			}
			
		})
	}
})