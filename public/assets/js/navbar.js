// console.log("hello")

let navItem = document.querySelector('#navSession');

let userToken = localStorage.getItem('token')

//create a control structure that will determine the display inside the navbar if there is a user currently logged in the app.

if (!userToken) {
	navItem = `
	<li class="nav-item">
		<a href="./login.html" class="nav-link">Login</a>
	</li>
	`
} else {
	navItem = `
	<li class="nav-item">
		<a href="./logout.html" class="nav-link">Logout</a>
	</li>
	`
}