// create our add course page

let formSubmit = document.querySelector('#createCourse')

let header = document.querySelector('.header');
let hamburgerMenu = document.querySelector('.hamburger-menu');


window.addEventListener('scroll', (e) => {
   let windowPosition = window.scrollY > 500;
   header.classList.toggle('active', windowPosition);
})

hamburgerMenu.addEventListener('click', () => {
   header.classList.toggle('menu-open');
})

// create a logic that will make sure that the needed information is sufficient from our form component. And make sure to inform the user what went wrong.
// acquire an event that will be applied in our form component.
// create a subfunction inside the method to describe the procedure/action that will take place upon triggering the event.
formSubmit.addEventListener("submit", (mangyayari) => {
	mangyayari.preventDefault() /*this will avoid page redirection*/

	// lets target the values of each component inside the forms
	let name = document.querySelector("#courseName").value
	let cost = document.querySelector("#coursePrice").value
	let desc = document.querySelector("#courseDesc").value

	// lets create a checker to see if we were able to capture the values of each input fields.

	/*console.log(name)
	console.log(cost)
	console.log(desc)*/

	// send a request to the backend project to process the data for creating a new entry inside our courses collection.	
	if (name !== "" && cost !== "" && desc !== "") {

		fetch('https://polar-meadow-85913.herokuapp.com/api/courses/course-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: name
			})
		}).then(res => {
			return res.json()
		}).then(courseData => {

			if (courseData === false) {

				// upon creating this fetch api request we are instantiating a promise
			fetch('https://polar-meadow-85913.herokuapp.com/api/courses/create', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					// what are the properties of the document that the use needs to fill?
					name: name,
					description: desc,
					price: cost
				})
			}).then(res => {
				console.log(res)
				return res.json()
			}).then(info => {
				// create a control structure that will describe the response of the UI to the client.
				if (info === true) {
					Swal.fire(
					  'Good job!',
					  'Course Created Successfully!',
					  'success'
					)
				} else {
					Swal.fire("Not Complete!")
				}
			})

			} else {
				Swal.fire({
   	  			icon: "error",
				text: "Course Already Exists!"
   	  		})
			}
		})	
	}else {
		Swal.fire({
			icon: "error",
			text: "Course Not Complete!"
		})
	}
	

})